/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.service.integration.sap.ecc.mappings.sharaf.component;

import java.math.BigDecimal;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

@ApplicationScoped
@IdocType("E1WPZ02_EXTENSION")
public class CashUpWPUTABLineE1WPZ02ExtensionTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "FDF3359681C04B1BB3302FFDB473E383";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getCreditCardDetail() {
    return StringUtils.defaultIfBlank((String) getProperties().get("KARTENNR"), StringUtils.EMPTY);
  }

  public String getCreditCardAuth() {
    return StringUtils.defaultIfBlank((String) getProperties().get("ZUONR"), StringUtils.EMPTY);
  }
  
  public String getCreditNoteNumber() {
    return StringUtils.defaultIfBlank((String) getProperties().get("REFERENZ2"), StringUtils.EMPTY);
  }

  public String getExpected() {
    BigDecimal expected = (BigDecimal) getProperties().get("REFERENZ1");
    String value = StringUtils.EMPTY;
    if (expected != null) {
      value = expected.toString();
    }
    return value;
  }

  public String getReferenceSign() {
    return StringUtils.defaultIfBlank((String) getProperties().get("SETTL"), StringUtils.EMPTY);
  }

  public String getConversionRate() {
    //BigDecimal conversionRate = (BigDecimal) getProperties().get("REFERENZ2");
    //String value = StringUtils.EMPTY;
    //if (conversionRate != null) {
    //  value = conversionRate.toString();
    //}
    //return value;
    return StringUtils.defaultIfBlank((String) getProperties().get("REFERENZ2"), StringUtils.EMPTY);
  }

}
