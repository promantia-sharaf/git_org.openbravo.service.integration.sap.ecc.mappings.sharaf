/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.sharaf.javamapper;

import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.javamapper.InvoiceJavaPropertyMappingHandler;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * Extends the standard JavaPropertyMappingHandler for the Invoice entity
 * 
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = "Invoice")
public class InvoiceCustomJavaPropertyMappingHandler extends InvoiceJavaPropertyMappingHandler {

  @Override
  public Object getValueOfProperty(Invoice invoice, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SEGMENT:
      return super.getPaymentMethodLines(invoice);
    case MappingConstants.SAP_INVOICE_USER_NAME:
      return getCashier(invoice);
    case MappingConstants.SAP_INVOICEDATE_DATE:
      if (MappingUtils.getSalesOrderFromIvoice(invoice).getPOSSBusinessDate() != null) {
        return MappingUtils.getSalesOrderFromIvoice(invoice).getPOSSBusinessDate();
      } else {
        return MappingUtils.getSalesOrderFromIvoice(invoice).getOrderDate();
      }
    default:
      return super.getValueOfProperty(invoice, mappingName);
    }
  }

  private String getCashier(Invoice invoice) {
    String cashier = null;
    if (invoice.getCreatedBy().getBusinessPartner() != null) {
      cashier = invoice.getCreatedBy().getBusinessPartner().getSearchKey();
    }
    return cashier;
  }

  @Override
  protected void processPaymentDetail(SynchronizableBusinessObject sbo,
      FIN_PaymentScheduleDetail paymentDetail) {
    super.processPaymentDetail(sbo, paymentDetail);
    sbo.addProperty(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_CCDETAIL, paymentDetail
        .getPaymentDetails().getFinPayment().getSharccCardDetails());
    sbo.addProperty(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_CCAUTHCODE, paymentDetail
        .getPaymentDetails().getFinPayment().getSharccCardAuthcode());
    sbo.addProperty(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SHOWCC, true);
  }

  @Override
  public int getPriority() {
    return 90;
  }
}
