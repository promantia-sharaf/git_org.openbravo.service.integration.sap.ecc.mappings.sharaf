/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.sharaf.javamapper;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceLineOffer;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.javamapper.InvoiceLineJavaPropertyMappingHandler;
import org.openbravo.service.integration.sap.ecc.mappings.sharaf.utils.DateUtils;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * Extends the standard JavaPropertyMappingHandler for the InvoiceLine entity
 * 
 * See templates E1WPB02_EXTENSION.ftl and E1WPB03_EXTENSION.ftl
 *
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = "InvoiceLine")
public class InvoiceLineCustomJavaPropertyMappingHandler extends
    InvoiceLineJavaPropertyMappingHandler {

  private static final String SELLER_IS_NOT_PROPERLY_CONFIGURED = "The seller does not have a business partner associated. Please configure the seller properly";
  private static final String WAREHOUSE_DISTRIBUTION_TYPE = "WD";
  private static final String AKTIONSNR = "AKTIONSNR";
  private static final String P_DEL_ADD2 = "P_DEL_ADD2";
  private static final String CONTACT2 = "CONTACT2";
  private static final String VTWEG = "VTWEG";
  private static final String WEBORDER = "WEBORDER";
  private static final String VERKAEUFER = "VERKAEUFER";
  private static final String PSTYV = "PSTYV";
  private static final String RET_RES = "RET_RES";
  private static final String UEPOS = "UEPOS";
  private static final String POSNR = "POSNR";
  private static final String VRKME = "VRKME";
  private static final String AIRMIPT = "AIRMIPT";
  private static final String AIRMNO = "AIRMNO";
  private static final String RFID = "RFID";
  private static final String LGORT = "LGORT";
  private static final String WERKS = "WERKS";
  private static final String P_DEL_DATE = "P_DEL_DATE";
  private static final String REMARKS = "REMARKS";
  private static final String CONTACT1 = "CONTACT1";
  private static final String P_DEL_ADD1 = "P_DEL_ADD1";
  private static final String ZZACT_RCDT = "ZZACT_RCDT";
  private static final String ZZACT_RCPT = "ZZACT_RCPT";
  private static final String VENDOR = "VENDOR";
  private static final String TEDT = "TEDT";
  private static final String TSDT = "TSDT";
  private static final String VKORG = "VKORG";
  private static final String AUART = "AUART";
  private static final String ECUST = "ECUST";
  private static final String INSTANT_REDEMPTION_TYPE_B = "IRB";
  private static final String INSTANT_REDEMPTION_TYPE_A = "IRA";
  private static final String SERVICE = "S";
  private static final String DEFAULT_DELIVERY_CONDITION = "IM";
  private static final String SAP_IS_NEGATIVE_SIGN = "-";
  private static final String SAP_IS_POSITIVE_SIGN = "+";

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> propertySorting = super.getPropertySorting();
    propertySorting.put(521, AUART);
    propertySorting.put(531, VKORG);
    propertySorting.put(541, TSDT);
    propertySorting.put(551, TEDT);
    propertySorting.put(561, ZZACT_RCPT);
    propertySorting.put(571, ZZACT_RCDT);
    propertySorting.put(581, P_DEL_ADD1);
    propertySorting.put(591, CONTACT1);
    propertySorting.put(601, REMARKS);
    propertySorting.put(611, P_DEL_DATE);
    propertySorting.put(621, WERKS);
    propertySorting.put(631, LGORT);
    propertySorting.put(641, RFID);
    propertySorting.put(651, AIRMNO);
    propertySorting.put(661, AIRMIPT);
    propertySorting.put(671, VRKME);
    propertySorting.put(681, POSNR);
    propertySorting.put(691, UEPOS);
    propertySorting.put(701, RET_RES);
    propertySorting.put(711, PSTYV);
    propertySorting.put(721, VERKAEUFER);
    propertySorting.put(731, WEBORDER);
    propertySorting.put(741, VTWEG);
    propertySorting.put(751, CONTACT2);
    propertySorting.put(761, P_DEL_ADD2);
    propertySorting.put(771, AKTIONSNR);
    propertySorting.put(781, ECUST);
    propertySorting.put(791, VENDOR);
    return propertySorting;
  }

  @Override
  public Object getValueOfProperty(InvoiceLine bob, String mappingName) {
    switch (mappingName) {
    case AUART:
      return getDocumentType(bob);
    case VKORG:
      return getCountry(bob);
    case TSDT:
      return getCreationDate(bob);
    case TEDT:
      return getEndDate(bob);
    case ZZACT_RCPT:
      return getOriginalTicketNumber(bob);
    case ZZACT_RCDT:
      return getOriginalTicketDate(bob);
    case P_DEL_ADD1:
      return getDeliveryAddress1(bob);
    case P_DEL_ADD2:
      return getDeliveryAddress2(bob);
    case CONTACT1:
      return getPhoneNumber(bob);
    case REMARKS:
      return getTicketComments(bob);
    case P_DEL_DATE:
      return getTicketDeliveryDate(bob);
    case WERKS:
      return getStore(bob);
    case LGORT:
      return getStockType(bob);
    case RFID:
      return getRfidCode(bob);
    case AIRMNO:
      return getAirmilesCardNumber(bob);
    case AIRMIPT:
      return getAirmilesEarnedPoints(bob);
    case VRKME:
      return getUnitOfMeasure(bob);
    case POSNR:
      return getLineSequenceNumber(bob);
    case UEPOS:
      return getParentRecord(bob);
    case RET_RES:
      return getReturnReason(bob);
    case PSTYV:
      return getDeliveryType(bob);
    case VERKAEUFER:
      return getSalesRepresentative(bob);
    case MappingConstants.SAP_TAX_RATE:
      return null;
    case WEBORDER:
      return getWebOrder(bob);
    case VTWEG:
      return getDistributionChannel(bob);
    case CONTACT2:
      return getPhoneNumber(bob);
    case AKTIONSNR:
      return getPromotionCode(bob);
    case MappingConstants.SAP_INVOICELINE_GROSS_AMOUNT:
      return getCorrectedGrossAmount(bob);
    case MappingConstants.SAP_INVOICELINE_DISCOUNT_SEGMENT:
      return getStandardAndSharafDiscounts(bob);
    case MappingConstants.SAP_INVOICELINE_TAX_AMOUNT:
      return getTaxAmount(bob);
    case ECUST:
      return StringUtils.defaultIfBlank(
          bob.getSalesOrderLine().getSalesOrder().getCustshaCustomerName(), StringUtils.EMPTY);
    case VENDOR:
      return StringUtils.defaultIfBlank(bob.getCustshaVendorCode(), StringUtils.EMPTY);
    default:
      return super.getValueOfProperty(bob, mappingName);
    }
  }

  private BigDecimal getTaxAmount(InvoiceLine invoiceLine) {
    if (!invoiceLine.getInvoiceLineTaxList().isEmpty()) {
      return invoiceLine.getInvoiceLineTaxList().get(0).getTaxAmount().abs();
    }
    return BigDecimal.ZERO;
  }

  private String getPromotionCode(InvoiceLine bob) {
    String promotionCode = null;
    if (bob.getSalesOrderLine().getCustdisOffer() != null) {
      promotionCode = bob.getSalesOrderLine().getCustdisOffer().getPrintName();
    }
    return promotionCode;
  }

  private String getDistributionChannel(InvoiceLine bob) {
    return bob.getSalesOrderLine().getSalesOrder().getOrganization().getSapobmpVtweg();
  }

  private String getWebOrder(InvoiceLine bob) {
    return bob.getSalesOrderLine().getSalesOrder().getCustshaWebsiterefno();
  }

  private String getDocumentType(InvoiceLine bob) {
    if (bob.getSalesOrderLine().getSalesOrder().getCustsdtDocumenttype() != null) {
      return bob.getSalesOrderLine().getSalesOrder().getCustsdtDocumenttype().getSearchKey();
    }
    return null;
  }

  private Object getCountry(InvoiceLine bob) {
    return bob.getSalesOrderLine().getSalesOrder().getOrganization().getSapobmpVkorg();
  }

  private String getCreationDate(InvoiceLine bob) {
    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    return dateFormat
        .format(DateUtils.convertToUTC(bob.getSalesOrderLine().getSalesOrder().getCreationDate()));
  }

  private Object getEndDate(InvoiceLine bob) {
    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    return dateFormat
        .format(DateUtils.convertToUTC(bob.getSalesOrderLine().getSalesOrder().getCreationDate()));
  }

  private String getOriginalTicketNumber(InvoiceLine bob) {
    try {
      if (bob.getSalesOrderLine().getSalesOrder().getDocumentType().isReturn()) {
        return bob.getSalesOrderLine().getGoodsShipmentLine().getSalesOrderLine().getSalesOrder()
            .getDocumentNo();
      }
    } catch (NullPointerException npe) {
      // Do nothing, allows returning null;
    }
    return null;
  }

  private String getOriginalTicketDate(InvoiceLine bob) {
    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    try {
      if (bob.getSalesOrderLine().getSalesOrder().getDocumentType().isReturn()) {
        return dateFormat.format(DateUtils.convertToUTC(bob.getSalesOrderLine()
            .getGoodsShipmentLine().getSalesOrderLine().getSalesOrder().getOrderDate()));
      }
    } catch (NullPointerException npe) {
      // Do nothing, allows returning null;
    }
    return null;
  }

  private String getDeliveryAddress1(InvoiceLine bob) {
    String address1 = StringUtils.EMPTY;
    String bpLocation = StringUtils.EMPTY;
    if (StringUtils.isNotBlank(bob.getSalesOrderLine().getSalesOrder().getCustshaCustomerAddress())) {
      address1 = StringUtils.substring(bob.getSalesOrderLine().getSalesOrder()
          .getCustshaCustomerAddress(), 0, 40);
    } else {
      bpLocation = bob.getInvoice().getPartnerAddress().getLocationAddress().getIdentifier();

      if (StringUtils.isNotEmpty(bpLocation)) {
        address1 = StringUtils.substring(bpLocation, 0, 40);
      }
    }
    return address1;
  }

  private String getDeliveryAddress2(InvoiceLine bob) {
    String address2 = StringUtils.EMPTY;
    String bpLocation = StringUtils.EMPTY;
    if (StringUtils.isNotBlank(bob.getSalesOrderLine().getSalesOrder().getCustshaCustomerAddress())) {
      address2 = StringUtils.substring(bob.getSalesOrderLine().getSalesOrder()
          .getCustshaCustomerAddress(), 40);
    } else {
      bpLocation = bob.getInvoice().getPartnerAddress().getLocationAddress().getIdentifier();
      if (StringUtils.isNotEmpty(bpLocation)) {
        address2 = StringUtils.substring(bpLocation, 40);
      }
    }
    return address2;
  }

  private String getPhoneNumber(InvoiceLine bob) {
    String phone = StringUtils.EMPTY;
    if (StringUtils.isNotBlank(bob.getSalesOrderLine().getSalesOrder().getCustshaCustomerPhone())) {
      phone = bob.getSalesOrderLine().getSalesOrder().getCustshaCustomerPhone();
    } else {
      User contact = getContact(bob.getSalesOrderLine().getSalesOrder().getBusinessPartner());
      if (contact != null) {
        phone = contact.getPhone();
      }
    }
    return phone;
  }

  private User getContact(BusinessPartner businessPartner) {
    StringBuilder hql = new StringBuilder();
    hql.append("from ADUser u where u.id = (select max(e.id) from ADUser e where e.businessPartner.id = :bPartnerId)");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hql.toString());
    query.setParameter("bPartnerId", businessPartner.getId());
    return (User) query.uniqueResult();
  }

  private String getTicketComments(InvoiceLine bob) {
    return bob.getSalesOrderLine().getSalesOrder().getDescription();
  }

  private Object getTicketDeliveryDate(InvoiceLine bob) {
    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    if (bob.getSalesOrderLine().getCUSTDELDeliveryTime() != null) {
      return dateFormat.format(bob.getSalesOrderLine().getCUSTDELDeliveryTime());
    }
    return null;
  }

  private String getStore(InvoiceLine bob) {
    /*
     * if (WAREHOUSE_DISTRIBUTION_TYPE.equals(getDeliveryType(bob))) { return
     * MappingUtils.getSAPCode(getWarehouse(bob)); }
     */
    if (bob.getSalesOrderLine().getWarehouse() != null) {
      return MappingUtils.getSAPCode(bob.getSalesOrderLine().getWarehouse());
    }
    return MappingUtils.getSAPFiliale(bob.getSalesOrderLine().getSalesOrder());
  }

  /*
   * private Warehouse getWarehouse(InvoiceLine invoiceLine) { Warehouse warehouse = null;
   * List<OrgWarehouse> organizationWarehouseList = invoiceLine.getOrganization()
   * .getOrganizationWarehouseList(); if (!organizationWarehouseList.isEmpty()) { warehouse =
   * organizationWarehouseList.get(0).getWarehouse(); } return warehouse; }
   */

  private String getStockType(InvoiceLine bob) {
    return bob.getSalesOrderLine().getCUSTSHAStockType();
  }

  private String getRfidCode(InvoiceLine bob) {
    return bob.getSalesOrderLine().getObposEpccode();
  }

  private String getAirmilesCardNumber(InvoiceLine bob) {
    return bob.getSalesOrderLine().getSalesOrder().getCustarmCardnumber();
  }

  private String getAirmilesEarnedPoints(InvoiceLine bob) {
    Long earnedPoints = bob.getSalesOrderLine().getCustarmEarnedpoints();
    if (earnedPoints != null) {
      return earnedPoints.toString();
    }
    return null;
  }

  private String getUnitOfMeasure(InvoiceLine bob) {
    return bob.getUOM().getEDICode();
  }

  private String getLineSequenceNumber(InvoiceLine bob) {
    return bob.getLineNo().toString();
  }

  private String getParentRecord(InvoiceLine bob) {
    String lineNo = null;
    final boolean productTypeIsService = bob.getSalesOrderLine().getProduct().getProductType()
        .equals(SERVICE);
    final boolean productIsPartOfPromotion = bob.getSalesOrderLine().getCustdisOrderline() != null;

    if (productTypeIsService) {
      lineNo = getLineNoOfAssociatedProduct(bob.getSalesOrderLine());
    } else if (productIsPartOfPromotion) {
      lineNo = getLineNoOfMainProductInPromotion(bob);
    }
    return lineNo;
  }

  private String getLineNoOfMainProductInPromotion(InvoiceLine bob) {
    return bob.getSalesOrderLine().getCustdisOrderline().getLineNo().toString();
  }

  private String getLineNoOfAssociatedProduct(OrderLine orderLine) {
    String lineNo = null;
    if (!orderLine.getOrderlineServiceRelationList().isEmpty()) {
      lineNo = orderLine.getOrderlineServiceRelationList().get(0).getOrderlineRelated().getLineNo()
          .toString();
    }
    return lineNo;
  }

  private String getReturnReason(InvoiceLine bob) {
    if (bob.getSalesOrderLine().getReturnReason() != null) {
      return bob.getSalesOrderLine().getReturnReason().getSearchKey();
    }
    return null;
  }

  private String getDeliveryType(InvoiceLine bob) {
    return StringUtils.defaultIfBlank(bob.getSalesOrderLine().getCUSTDELDeliveryCondition(),
        DEFAULT_DELIVERY_CONDITION);
  }

  private String getSalesRepresentative(InvoiceLine bob) {
    if (bob.getSalesOrderLine().getShasrSalesrep() != null) {
      if (bob.getSalesOrderLine().getShasrSalesrep().getBusinessPartner() != null) {
        return bob.getSalesOrderLine().getShasrSalesrep().getBusinessPartner().getSearchKey();
      }
      throw new OBException(SELLER_IS_NOT_PROPERLY_CONFIGURED);
    } else if (bob.getSalesOrderLine().getSalesOrder().getSalesRepresentative() != null) {
      if (bob.getSalesOrderLine().getSalesOrder().getSalesRepresentative().getBusinessPartner() != null) {
        return bob.getSalesOrderLine().getSalesOrder().getSalesRepresentative()
            .getBusinessPartner().getSearchKey();

      }
      throw new OBException(SELLER_IS_NOT_PROPERLY_CONFIGURED);
    }
    throw new OBException(SELLER_IS_NOT_PROPERLY_CONFIGURED);
  }

  @SuppressWarnings("unchecked")
  private List<SynchronizableBusinessObject> getStandardAndSharafDiscounts(InvoiceLine bob) {
    List<SynchronizableBusinessObject> sbos = (List<SynchronizableBusinessObject>) super
        .getValueOfProperty(bob, MappingConstants.SAP_INVOICELINE_DISCOUNT_SEGMENT);

    PriceAdjustment sharafOffer = bob.getSalesOrderLine().getCustdisOffer();
    final boolean thereIsNotDiscounts = sbos.isEmpty();

    if (thereIsNotDiscounts && sharafOffer != null) {
      SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
      sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_SAP_CODE_FIELD,
          getDiscountSapCode(bob.getProduct(), sharafOffer));
      sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_AMOUNT,
          getSharafDiscountValue(bob, sharafOffer));
      sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_SIGN, getSignForSalesOrReturns(bob));
      sbos.add(sbo);
    }
    return sbos;
  }

  private BigDecimal getSharafDiscountValue(InvoiceLine bob, PriceAdjustment sharafOffer) {
    BigDecimal discountValue = bob.getLineNetAmount();
    if (thereIsOfferProductForOfferAndProduct(sharafOffer, bob.getProduct())) {
      discountValue = getRedemptionAmount(bob);
    }
    return discountValue;
  }

  private boolean thereIsOfferProductForOfferAndProduct(PriceAdjustment sharafOffer, Product product) {
    StringBuilder hqlQuery = new StringBuilder(
        "select 1 from PricingAdjustmentProduct p where p.priceAdjustment.id = :offerId and p.product.id = :productId ");
    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());
    query.setParameter("offerId", sharafOffer.getId());
    query.setParameter("productId", product.getId());
    query.setMaxResults(1);

    return query.uniqueResult() != null;
  }

  @SuppressWarnings("unchecked")
  private BigDecimal getCorrectedGrossAmount(InvoiceLine bob) {
    BigDecimal amount = getNetAmountPlusOffer(bob);

    // if the line has amount there is no need to take it from the discount
    if (!(amount.compareTo(BigDecimal.ZERO) == 0)) {
      return amount;
    }
    List<SynchronizableBusinessObject> sbos = (List<SynchronizableBusinessObject>) super
        .getValueOfProperty(bob, MappingConstants.SAP_INVOICELINE_DISCOUNT_SEGMENT);
    PriceAdjustment sharafOffer = bob.getSalesOrderLine().getCustdisOffer();
    final boolean thereIsNotDiscounts = sbos.isEmpty();

    if (thereIsNotDiscounts && sharafOffer != null) {
      amount = getRedemptionAmount(bob);
    }
    return amount;
  }

  private BigDecimal getNetAmountPlusOffer(InvoiceLine bob) {
    return bob.getLineNetAmount().add(getTotalDiscounts(bob)).abs();
  }

  private BigDecimal getTotalDiscounts(InvoiceLine bob) {

    OBCriteria<InvoiceLineOffer> lineOfferCriteria = OBDal.getInstance().createCriteria(
        InvoiceLineOffer.class);
    lineOfferCriteria.add(Restrictions.eq(InvoiceLineOffer.PROPERTY_INVOICELINE, bob));

    ProjectionList projection = Projections.projectionList();
    projection.add(Projections.sum(InvoiceLineOffer.PROPERTY_TOTALAMOUNT));

    lineOfferCriteria.setProjection(projection);
    lineOfferCriteria.setMaxResults(1);

    return (BigDecimal) ObjectUtils.defaultIfNull((BigDecimal) lineOfferCriteria.uniqueResult(),
        BigDecimal.ZERO);
  }

  private BigDecimal getRedemptionAmount(InvoiceLine bob) {
    final boolean lineHasZeroAmount = bob.getLineNetAmount().compareTo(BigDecimal.ZERO) == 0;
    final boolean redemptionTypeIsAOrB = StringUtils.equals(bob.getSalesOrderLine()
        .getCustdisIrType(), INSTANT_REDEMPTION_TYPE_A)
        || StringUtils
            .equals(bob.getSalesOrderLine().getCustdisIrType(), INSTANT_REDEMPTION_TYPE_B);
    final boolean redemptionPaymentHasValue = bob.getSalesOrderLine().getCustdisRedemptionAmount() != null;

    BigDecimal discountAmount = BigDecimal.ZERO;

    if (lineHasZeroAmount && redemptionTypeIsAOrB && redemptionPaymentHasValue) {
      discountAmount = bob.getSalesOrderLine().getCustdisRedemptionAmount();
    }
    return discountAmount;
  }

  @Override
  protected void processInvoiceLineOffer(SynchronizableBusinessObject sbo,
      InvoiceLineOffer lineOffer) {
    if (lineOffer.getTotalAmount().compareTo(BigDecimal.ZERO) != 0) {
      sbo.addProperty(
          MappingConstants.SAP_INVOICELINE_DISCOUNT_SAP_CODE_FIELD,
          getDiscountSapCode(lineOffer.getInvoiceLine().getProduct(),
              lineOffer.getPriceAdjustment()));
      sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_AMOUNT, lineOffer.getTotalAmount()
          .abs());
      sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_SIGN,
          getSignForLineDiscount(lineOffer));
    }
  }

  private String getDiscountSapCode(Product product, PriceAdjustment offer) {
    String offerDiscountSapCode = getOfferDiscountSapCode(product, offer);
    if (offerDiscountSapCode == null) {
      offerDiscountSapCode = MappingUtils.getSAPCode(offer);
    }
    return offerDiscountSapCode;
  }

  private String getOfferDiscountSapCode(Product product, PriceAdjustment offer) {
    StringBuilder hqlQuery = new StringBuilder(
        "select p.sapsharSapcode from PricingAdjustmentProduct p ");
    hqlQuery.append("where p.priceAdjustment.id = :offerId and p.product.id = :productId ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());
    query.setParameter("offerId", offer.getId());
    query.setParameter("productId", product.getId());
    query.setMaxResults(1);

    return (String) query.uniqueResult();
  }

  @Override
  public int getPriority() {
    return 90;
  }

  @Override
  protected boolean itemShouldBeExported(InvoiceLine invoiceLine) {
    return invoiceLine.getSalesOrderLine() != null;
  }

}
