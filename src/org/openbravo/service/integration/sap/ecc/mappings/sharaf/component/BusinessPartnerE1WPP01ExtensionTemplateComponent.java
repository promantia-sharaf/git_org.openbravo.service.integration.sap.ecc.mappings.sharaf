/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.service.integration.sap.ecc.mappings.sharaf.component;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

@ApplicationScoped
@IdocType("E1WPP01_EXTENSION")
public class BusinessPartnerE1WPP01ExtensionTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "1DBA8D5874C447CC88936FD8581EB5AD";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getVatCode() {
    return (String) getProperties().get("VATCODE");
  }

}
