/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.sharaf.javamapper;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.javamapper.BusinessPartnerJavaPropertyMappingHandler;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler to extend the standard JavaPropertyMappingHandler for the
 * BusinessPartner entity
 * 
 * It adds the VATCODE tag to the main property map, and it also includes the EMAIL tag in the
 * property map of the business partner locations
 * 
 * See templates E1WPP01_EXTENSION.ftl and E1WPP03_EXTENSION.ftl
 *
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = "BusinessPartner")
public class BusinessPartnerCustomJavaPropertyMappingHandler extends
    BusinessPartnerJavaPropertyMappingHandler {

  private static final String DEFAULT_EMAIL_SHARAF = "na@sharafdg.com";

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> propertySorting = super.getPropertySorting();
    propertySorting.put(100, "VATCODE");
    return propertySorting;
  }

  @Override
  public Object getValueOfProperty(BusinessPartner bob, String mappingName) {
    switch (mappingName) {
    case "VATCODE":
      return getBusinessPartnerVatCode(bob);
    case MappingConstants.SAP_WP_PER_ADDRESS_SEGMENT:
      return getBusinessPartnerAddressWithEmail(bob);
    default:
      return super.getValueOfProperty(bob, mappingName);
    }
  }

  @SuppressWarnings("unchecked")
  private Object getBusinessPartnerAddressWithEmail(BusinessPartner bob) {
    Object originalLocations = super.getValueOfProperty(bob,
        MappingConstants.SAP_WP_PER_ADDRESS_SEGMENT);
    String email = getEmail(bob);
    if (StringUtils.isBlank(email) || originalLocations == null) {
      return originalLocations;
    }
    List<SynchronizableBusinessObject> sboLocations = (List<SynchronizableBusinessObject>) originalLocations;
    for (SynchronizableBusinessObject sbo : sboLocations) {
      sbo.getProperties().put("BEMERKUNG", email);
      if (sbo.getProperties().containsKey(MappingConstants.SAP_POSTAL_CODE)) {
        sbo.getProperties().put("ABTEILUNG",
            sbo.getProperties().get(MappingConstants.SAP_POSTAL_CODE));
        sbo.getProperties().remove(MappingConstants.SAP_POSTAL_CODE);
      }
      if (sbo.getProperties().containsKey(MappingConstants.SAP_WP_PER_PHONE)) {
        sbo.getProperties()
            .put("FIRMA", sbo.getProperties().get(MappingConstants.SAP_WP_PER_PHONE));
      }
      if (sbo.getProperties().containsKey(MappingConstants.SAP_ADDRESS)) {
        sbo.addProperty("FUNKTION", sbo.getProperties().get(MappingConstants.SAP_ADDRESS));
        sbo.getProperties().remove(MappingConstants.SAP_ADDRESS);
      }
    }
    return sboLocations;
  }

  @Override
  protected Location getValidLocationFromBusinessPartner(BusinessPartner bob) {
    OBCriteria<Location> locationCriteria = OBDal.getInstance().createCriteria(Location.class);
    locationCriteria.add(Restrictions.eq(Location.PROPERTY_BUSINESSPARTNER, bob));
    locationCriteria.add(Restrictions.eq(Location.PROPERTY_INVOICETOADDRESS, true));
    locationCriteria.addOrderBy(Location.PROPERTY_UPDATED, false);
    locationCriteria.setMaxResults(1);
    return (Location) locationCriteria.uniqueResult();
  }

  private String getEmail(BusinessPartner bob) {
    List<User> contacts = bob.getADUserList();
    if (contacts.isEmpty()) {
      return "";
    }
    User contact = contacts.get(0);
    String email = (contact.getEmail() != null) ? contact.getEmail().trim() : "";
    if (!email.isEmpty() && !EmailValidator.getInstance().isValid(email)) {
      email = DEFAULT_EMAIL_SHARAF;
    }
    return email;
  }

  private Object getBusinessPartnerVatCode(BusinessPartner bob) {
    return bob.getTaxID();
  }

  @Override
  public int getPriority() {
    // return a lower number than the standard java property mapper (100) to ensure the custom one
    // is selected
    return 10;
  }

}
