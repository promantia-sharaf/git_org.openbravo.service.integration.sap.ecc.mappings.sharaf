package org.openbravo.service.integration.sap.ecc.mappings.sharaf.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
  public static Date convertToUTC(Date localTime) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(localTime);
    int gmtMillisecondOffset = (calendar.get(Calendar.ZONE_OFFSET) + calendar
        .get(Calendar.DST_OFFSET));
    calendar.add(Calendar.MILLISECOND, -gmtMillisecondOffset);

    return calendar.getTime();
  }
}