/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.sharaf.exporter;

import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.exporter.CashUpWPUTABExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = "SAPOBMP_CashUp")
public class CashUpWPUTABCustomExporter extends CashUpWPUTABExporter {

  @Override
  public String getSynchronizationQuery() {
    StringBuilder query = new StringBuilder();
    query.append(" SELECT cs.id from SAPOBMP_CashUp cs ");
    query.append(" WHERE cs.$incrementalUpdateCriteria ");
    query.append(" AND cs.isprocessedbo = true ");
    query.append(" AND cs.$clientCriteria ");
    query
        .append(" AND EXISTS (select 1 from InvoiceLine il inner join il.salesOrderLine ol inner join ol.salesOrder so ");
    query.append("where so.$clientCriteria ");
    query.append("and so.obposAppCashup is not null ");
    query.append("and so.obposAppCashup = cs.id ");
    query.append("and il.invoice.salesOrder is not null) ");

    return query.toString();
  }

  @Override
  public int getPriority() {
    return 90;
  }

}
