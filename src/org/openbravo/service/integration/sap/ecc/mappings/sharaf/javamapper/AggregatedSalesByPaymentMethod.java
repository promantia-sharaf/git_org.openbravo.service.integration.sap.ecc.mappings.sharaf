/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.service.integration.sap.ecc.mappings.sharaf.javamapper;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.CashUp;
import org.openbravo.service.integration.sap.ecc.mappings.javamapper.processor.SalesByPaymentMethodProcessor;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;

import org.apache.log4j.Logger;

public class AggregatedSalesByPaymentMethod extends SalesByPaymentMethodProcessor {

  private static Logger log = Logger.getLogger(AggregatedSalesByPaymentMethod.class);
  private CashUp cashUp;
  private Map<String, SynchronizableBusinessObject> extraSegments;

  public AggregatedSalesByPaymentMethod(CashUp cashUp) {
    this.cashUp = cashUp;
  }

  @Override
  protected ScrollableResults getSalesGroupedByPaymentMethod() {
    StringBuilder hql = new StringBuilder();
    hql.append(" select c.c_currency_id as m_currency, sum(ocsh.totalcounted) as totalCounted, ");
    hql.append(" sum(fpsd.paidamt) as totalPaid, ");
    hql.append(" pm.em_sapobmp_sapcode, fp.em_sharcc_card_details, fp.em_sharcc_card_authcode, ");
    hql.append(" sum(ocsh.totalsales - ocsh.totalreturns + ocsh.totaldeposits - ocsh.totaldrops) as expected, count(*), ");
    hql.append(" orgcur.c_currency_id as org_currency, ");
    hql.append(" p.name, case when fp.em_sharcc_card_details is null then null else o.documentno end, fp.em_custcn_creditnote_number ");
    hql.append(" from  ");
    hql.append(" obpos_paymentmethodcashup ocsh inner join  obpos_app_payment p on ocsh.obpos_app_payment_id = p.obpos_app_payment_id ");
    hql.append(" inner join obpos_app_payment_type pt on pt.obpos_app_payment_type_id = p.obpos_app_payment_type_id ");
    hql.append(" inner join fin_paymentmethod pm on pm.fin_paymentmethod_id = pt.fin_paymentmethod_id ");
    hql.append(" inner join c_currency c on c.c_currency_id = pt.c_currency_id ");
    hql.append(" inner join obpos_app_cashup csh on csh.obpos_app_cashup_id = ocsh.obpos_app_cashup_id ");
    hql.append(" inner join obpos_applications term on term.obpos_applications_id = csh.obpos_applications_id ");
    hql.append(" inner join ad_org org on org.ad_org_id = term.ad_org_id ");
    hql.append(" inner join c_currency orgcur on orgcur.c_currency_id = org.c_currency_id ");
    hql.append(" inner join FIN_Payment_Detail_V fpsd on fpsd.fin_paymentmethod_id = pm.fin_paymentmethod_id ");
    hql.append(" inner join fin_payment_schedule pso on fpsd.fin_payment_sched_ord_id = pso.fin_payment_schedule_id ");
    hql.append(" inner join c_order o on o.c_order_id = pso.c_order_id ");
    hql.append(" inner join fin_payment fp on fp.fin_payment_id = fpsd.fin_payment_id ");
    hql.append("  ");
    hql.append(" where  ");
    hql.append(" ocsh.obpos_app_cashup_id = :cashUpId ");
    hql.append(" and p.fin_financial_account_id = fpsd.fin_financial_account_id ");
    hql.append(" and o.em_obpos_app_cashup_id = ocsh.obpos_app_cashup_id ");
    hql.append("  ");
    hql.append(" group by ");
    hql.append(" c.c_currency_id, pm.em_sapobmp_sapcode, fp.em_sharcc_card_details, fp.em_sharcc_card_authcode, orgcur.c_currency_id, p.name, fp.em_custcn_creditnote_number ");
    hql.append(", case when fp.em_sharcc_card_details is null then null else fp.fin_payment_id end, case when fp.em_sharcc_card_details is null then null else o.documentno end");
    hql.append(", case when fp.em_custcn_creditnote_number is null then null else fp.fin_payment_id end");
    hql.append("  ");
    hql.append(" union all ");
    hql.append(" select c.c_currency_id as m_currency, ocsh.totalcounted as totalCounted, 0, pm.em_sapobmp_sapcode, null, null, ");
    hql.append(" (ocsh.totalsales - ocsh.totalreturns + ocsh.totaldeposits - ocsh.totaldrops) as expected, 1, orgcur.c_currency_id as org_currency, ");
    hql.append(" p.name, '', null ");
    hql.append(" from  ");
    hql.append(" (obpos_paymentmethodcashup ocsh inner join  obpos_app_payment p on ocsh.obpos_app_payment_id = p.obpos_app_payment_id ");
    hql.append(" inner join obpos_app_payment_type pt on pt.obpos_app_payment_type_id = p.obpos_app_payment_type_id ");
    hql.append(" inner join fin_paymentmethod pm on pm.fin_paymentmethod_id = pt.fin_paymentmethod_id ");
    hql.append(" inner join c_currency c on c.c_currency_id = pt.c_currency_id ");
    hql.append(" inner join obpos_app_cashup csh on csh.obpos_app_cashup_id = ocsh.obpos_app_cashup_id ");
    hql.append(" inner join obpos_applications term on term.obpos_applications_id = csh.obpos_applications_id ");
    hql.append(" inner join ad_org org on org.ad_org_id = term.ad_org_id ");
    hql.append(" inner join c_currency orgcur on orgcur.c_currency_id = org.c_currency_id ) ");
    hql.append("  ");
    hql.append(" where  ");
    hql.append(" ocsh.obpos_app_cashup_id = :cashUpId ");
    hql.append(" and ocsh.totalsales = 0 and ocsh.totalReturns = 0 and ocsh.totalcounted != 0 ");

   
    Query query = OBDal.getInstance().getSession().createSQLQuery(hql.toString());
    log.info(query.toString());
    query.setParameter("cashUpId", cashUp.getId());
    query.setFetchSize(1000);
    return query.scroll(ScrollMode.FORWARD_ONLY);
  }

  @Override
  public boolean hasSalesToProcess() {
    StringBuilder hqlQuery = new StringBuilder();
    hqlQuery
        .append("select count(*) as total from InvoiceLine il inner join il.salesOrderLine ol inner join ol.salesOrder so ");
    hqlQuery.append("where so.client.id = :clientId ");
    hqlQuery.append("and so.obposAppCashup is not null ");
    hqlQuery.append("and so.obposAppCashup = :cashUpId ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());
    query.setParameter("clientId", OBContext.getOBContext().getCurrentClient().getId());
    query.setParameter("cashUpId", cashUp.getId());

    Long total = (Long) query.uniqueResult();
    return total.longValue() > 0;
  }

  @Override
  protected void processPaymentScheduleDetail(SynchronizableBusinessObject sbo,
      ScrollableResults totalPaidByPaymentMethod) {
    String creditCardNumber = getCreditCardNumber(totalPaidByPaymentMethod);
    String creditCardAuthCode = getCreditCardAuthCode(totalPaidByPaymentMethod);
    sbo.addProperty("KARTENNR", creditCardNumber);
    sbo.addProperty("ZUONR", creditCardAuthCode);

    if (isCreditCard(totalPaidByPaymentMethod) || isCreditNote(totalPaidByPaymentMethod)) {
      sbo.addProperty("REFERENZ1", getTotalPaidAmount(totalPaidByPaymentMethod).abs());
      sbo.addProperty("SETTL", getSign(getTotalPaidAmount(totalPaidByPaymentMethod)));
      if (getExpected(totalPaidByPaymentMethod)
          .compareTo(getTotalCounted(totalPaidByPaymentMethod)) != 0) {
        addExtraSegment(totalPaidByPaymentMethod);
      }
      if (isCreditNote(totalPaidByPaymentMethod)) {
        sbo.addProperty("REFERENZ2", getCreditNoteNumber(totalPaidByPaymentMethod));
      } else {   
      sbo.addProperty("REFERENZ2", getSalesOrderNumber(totalPaidByPaymentMethod));
      }
    } else {
      sbo.addProperty("REFERENZ1", getExpected(totalPaidByPaymentMethod).abs());
      sbo.addProperty("SETTL", getSign(getExpected(totalPaidByPaymentMethod)));
    }

    //if (isForeignCurrency(totalPaidByPaymentMethod)) {
    //  sbo.addProperty("REFERENZ2", getForeignCurrencyConversionRate(totalPaidByPaymentMethod));
    //}
    
    

  }

  private String getSalesOrderNumber(ScrollableResults totalPaidByPaymentMethod) {
    final String documentNo = (String) totalPaidByPaymentMethod.get()[10];
    return (documentNo != null ? documentNo : "");
  }

  private String getCreditCardAuthCode(ScrollableResults totalPaidByPaymentMethod) {
    return (String) totalPaidByPaymentMethod.get()[5];
  }

  private String getCreditCardNumber(ScrollableResults totalPaidByPaymentMethod) {
    return (String) totalPaidByPaymentMethod.get()[4];
  }
  
  private String getCreditNoteNumber(ScrollableResults totalPaidByPaymentMethod) {
    return (String) totalPaidByPaymentMethod.get()[11];
  }


  private boolean isCreditCard(ScrollableResults totalPaidByPaymentMethod) {
    return totalPaidByPaymentMethod.get()[4] != null;
  }
  private boolean isCreditNote(ScrollableResults totalPaidByPaymentMethod) {
    return totalPaidByPaymentMethod.get()[11] != null;
  }

  private BigDecimal getTotalPaidAmount(ScrollableResults totalPaidByPaymentMethod) {
    return getValueInCurrencyPrecision((BigDecimal) totalPaidByPaymentMethod.get()[2],
        getCurrencyISOCode(totalPaidByPaymentMethod));
  }

  private String getSign(BigDecimal amount) {
    return amount.signum() < 0 ? "-" : "+";
  }

  private BigDecimal getExpected(ScrollableResults totalPaidByPaymentMethod) {
    return getValueInCurrencyPrecision(
        getValue((BigDecimal) totalPaidByPaymentMethod.get()[6], totalPaidByPaymentMethod),
        getCurrencyISOCode(totalPaidByPaymentMethod));
  }

  private void addExtraSegment(ScrollableResults totalPaidByPaymentMethod) {
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_WPUTAB_PAYMENT_METHOD_FIELD,
        getPaymentMethodSapCode(totalPaidByPaymentMethod));
    sbo.addProperty(MappingConstants.SAP_WPUTAB_CURRENCY,
        getCurrencyISOCode(totalPaidByPaymentMethod));
    final BigDecimal difference = getTotalCounted(totalPaidByPaymentMethod).subtract(
        getExpected(totalPaidByPaymentMethod));
    sbo.addProperty(MappingConstants.SAP_WPUTAB_TOTALPAID, difference.abs());
    sbo.addProperty(MappingConstants.SAP_WPUTAB_SIGN, getSign(difference));
    sbo.addProperty("KARTENNR", StringUtils.EMPTY);
    sbo.addProperty("ZUONR", StringUtils.EMPTY);
    sbo.addProperty("REFERENZ1", BigDecimal.ZERO);
    sbo.addProperty("SETTL", "+");

    storeExtraSegment(getPaymentMethodSapCode(totalPaidByPaymentMethod), sbo);
  }

  private void storeExtraSegment(String paymentMethodSapCode, SynchronizableBusinessObject sbo) {
    Map<String, SynchronizableBusinessObject> segments = getExtraSegments();
    if (!segments.containsKey(paymentMethodSapCode)) {
      segments.put(paymentMethodSapCode, sbo);
    }
  }

  private Map<String, SynchronizableBusinessObject> getExtraSegments() {
    if (this.extraSegments == null) {
      return this.extraSegments = new HashMap<>();
    }
    return this.extraSegments;
  }

  private boolean isForeignCurrency(ScrollableResults totalPaidByPaymentMethod) {
    return !StringUtils.equalsIgnoreCase(getCurrencyISOCode(totalPaidByPaymentMethod),
        getStoreCurrencyISOCode(totalPaidByPaymentMethod));
  }

  private BigDecimal getForeignCurrencyConversionRate(ScrollableResults totalPaidByPaymentMethod) {
    List<Object> parameters = new ArrayList<>();
    parameters.add(MappingUtils.getCurrencyByISOCode(getCurrencyISOCode(totalPaidByPaymentMethod))
        .getId());
    parameters.add(MappingUtils.getCurrencyByISOCode(
        getStoreCurrencyISOCode(totalPaidByPaymentMethod)).getId());
    parameters.add(cashUp.getCashUpdate());
    parameters.add("S");
    parameters.add(cashUp.getClient().getId());
    parameters.add(cashUp.getOrganization().getId());

    BigDecimal rate = null;

    try {
      rate = (BigDecimal) CallStoredProcedure.getInstance().call("obpos_currency_rate", parameters,
          null, false, true);
    } catch (IllegalStateException e) {
      throw new OBException(e);
    }

    return rate;
  }

  private String getStoreCurrencyISOCode(ScrollableResults totalPaidByPaymentMethod) {
    return OBDal.getInstance().getProxy(Currency.class, (String) totalPaidByPaymentMethod.get()[8])
        .getISOCode();
  }

  private BigDecimal getValue(BigDecimal value, ScrollableResults totalPaidByPaymentMethod) {
    final BigDecimal recordCount = new BigDecimal(
        ((BigInteger) totalPaidByPaymentMethod.get()[7]).toString());
    return value.divide(recordCount, RoundingMode.HALF_DOWN);
  }

  /**
   * This method returns ZAHLART value
   */
  @Override
  protected String getPaymentMethodSapCode(ScrollableResults totalPaidByPaymentMethod) {
    if (totalPaidByPaymentMethod.get()[3] == null) {
      throw new OBException(
          "Error: The cashup references a payment method that does not have a SAP Code "
              + totalPaidByPaymentMethod.get()[9]);
    }
    return (String) totalPaidByPaymentMethod.get()[3];
  }

  /**
   * This method returns WAEHRUNG value
   */
  @Override
  protected String getCurrencyISOCode(ScrollableResults totalPaidByPaymentMethod) {
    return OBDal.getInstance().getProxy(Currency.class, (String) totalPaidByPaymentMethod.get()[0])
        .getISOCode();
  }

  /**
   * This method returns SUMME value
   */
  @Override
  protected BigDecimal getTotalPaid(ScrollableResults totalPaidByPaymentMethod) {
    if (isCreditCard(totalPaidByPaymentMethod) || isCreditNote(totalPaidByPaymentMethod)) {
      return getTotalPaidAmount(totalPaidByPaymentMethod);
    } else {
      return getTotalCounted(totalPaidByPaymentMethod);
    }
  }

  private BigDecimal getTotalCounted(ScrollableResults totalPaidByPaymentMethod) {
    return getValueInCurrencyPrecision(
        getValue((BigDecimal) totalPaidByPaymentMethod.get()[1], totalPaidByPaymentMethod),
        getCurrencyISOCode(totalPaidByPaymentMethod));
  }

  /**
   * This method returns VORZEICHEN value
   */
  @Override
  protected String getTotalPaidSign(ScrollableResults totalPaidByPaymentMethod) {
    return getSign(getTotalPaid(totalPaidByPaymentMethod));
  }

  private BigDecimal getValueInCurrencyPrecision(final BigDecimal value,
      final String currencyISOCode) {
    Currency currency = MappingUtils.getCurrencyByISOCode(currencyISOCode);
    return value.setScale(currency.getStandardPrecision().intValue(), RoundingMode.HALF_DOWN);
  }

  @Override
  protected void addExtraSegments(List<SynchronizableBusinessObject> sbos) {
    for (Entry<String, SynchronizableBusinessObject> entry : getExtraSegments().entrySet()) {
      sbos.add(entry.getValue());
    }
  }
}
