/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.service.integration.sap.ecc.mappings.sharaf.component;

import java.math.BigDecimal;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

@ApplicationScoped
@IdocType("E1WPB02_EXTENSION")
public class InvoiceLineE1WPB02ExtensionTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "A057AB29DADA40A1BB428839A36E89CA";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getSalesRepresentative() {
    return getOptionalProperty("VERKAEUFER");
  }

  public String getDocumentType() {
    return getOptionalProperty("AUART");
  }

  public String getCountry() {
    return getOptionalProperty("VKORG");
  }

  public String getCreationDate() {
    return getOptionalProperty("TSDT");
  }

  public String getEndDate() {
    return getOptionalProperty("TEDT");
  }

  public String getOriginalTicketNumber() {
    return getOptionalProperty("ZZACT_RCPT");
  }

  public String getOriginalTicketDate() {
    return getOptionalProperty("ZZACT_RCDT");
  }

  public String getDeliveryAddress() {
    return getOptionalProperty("P_DEL_ADD1");
  }

  public String getPhoneNumber() {
    return getOptionalProperty("CONTACT1");
  }

  public String getTicketComments() {
    return getOptionalProperty("REMARKS");
  }

  public String getTicketDeliveryDate() {
    return getOptionalProperty("P_DEL_DATE");
  }

  public String getStore() {
    return getOptionalProperty("WERKS");
  }

  public String getStockType() {
    return getOptionalProperty("LGORT");
  }

  public String getRfidCode() {
    return getOptionalProperty("RFID");
  }

  public String getAirmilesCardNumber() {
    return getOptionalProperty("AIRMNO");
  }

  public String getAirmilesEarnedPoints() {
    return getOptionalProperty("AIRMIPT");
  }

  public String getUnitOfMeasure() {
    return getOptionalProperty("VRKME");
  }

  public String getLineSequenceNumber() {
    return getOptionalProperty("POSNR");
  }

  public String getParentRecord() {
    return getOptionalProperty("UEPOS");
  }

  public String getReturnReason() {
    return getOptionalProperty("RET_RES");
  }

  public String getDeliveryType() {
    return getOptionalProperty("PSTYV");
  }

  public String getOptionalProperty(String key) {
    return StringUtils.defaultIfBlank((String) getProperties().get(key), StringUtils.EMPTY);
  }

  public String getIsReturn() {
    return (String) getProperties().get(MappingConstants.SAP_INVOICELINE_RETURN_CODE);
  }

  public String getProduct() {
    return (String) getProperties().get(MappingConstants.SAP_INVOICELINE_PRODUCT_CODE_VALUE);
  }

  public String getProductCode() {
    return (String) getProperties().get(MappingConstants.SAP_INVOICELINE_PRODUCT_CODE);
  }

  public String getQuantity() {
    BigDecimal quantity = (BigDecimal) getProperties().get("MENGE");
    return quantity.toString();
  }

  public String getSign() {
    return (String) getProperties().get(MappingConstants.SAP_INVOICELINE_RETURN_SIGN);
  }

  public String getSerialNumber() {
    return getOptionalProperty(MappingConstants.SAP_INVOICE_SERIAL_NUMBER);
  }

  public String getWebOrder() {
    return getOptionalProperty("WEBORDER");
  }

  public String getDistributionChannel() {
    return getOptionalProperty("VTWEG");
  }

  public String getPhoneNumber2() {
    return getOptionalProperty("CONTACT2");
  }

  public String getDeliveryAddress2() {
    return getOptionalProperty("P_DEL_ADD2");
  }

  public String getPromotionCode() {
    return getOptionalProperty("AKTIONSNR");
  }
  
  public String getEndCustomerName() {
    return getOptionalProperty("ECUST");
  }

  public String getVendorCode() {
    return getOptionalProperty("VENDOR");
  }
}
