/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.service.integration.sap.ecc.mappings.sharaf.javamapper;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.retail.posterminal.OBPOSAppCashup;
import org.openbravo.retail.sessions.org.openbravo.retail.sessions.TerminalSessions;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.CashUp;
import org.openbravo.service.integration.sap.ecc.mappings.javamapper.CashUpWPUTABJavaPropertyMppingHandler;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for the CashUp entity of Sharaf
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = "SAPOBMP_CashUp")
public class CashUpWPUTABCustomJavaPropertyMppingHandler extends
    CashUpWPUTABJavaPropertyMppingHandler {

  private static final String RCVLAD = "RCVLAD";
  private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = super.getPropertySorting();
    properties.put(101, RCVLAD);
    return properties;
  }

  @Override
  public Object getValueOfProperty(CashUp cashUp, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_CASHUP_UPDATE:
      return getBusinessDate(cashUp);
    case MappingConstants.SAP_LINE_CASHUP_SEGMENT:
      return new AggregatedSalesByPaymentMethod(cashUp).getSalesByPaymentMethod();
    case MappingConstants.SAP_CASHUP_CASHIER:
      return getCashier(cashUp);
    case RCVLAD:
      return cashUp.getId();
    default:
      return super.getValueOfProperty(cashUp, mappingName);
    }
  }

  @Override
  protected String getCashier(CashUp cashUp) {
    OBPOSAppCashup obposCashUp = OBDal.getInstance().get(OBPOSAppCashup.class, cashUp.getId());

    String cashier = null;
    if (obposCashUp.getUserContact().getBusinessPartner() != null) {
      cashier = obposCashUp.getUserContact().getBusinessPartner().getSearchKey();
    }
    return cashier;
  }

  private String getBusinessDate(CashUp cashUp) {
    TerminalSessions tillStatus = getTillStatus(cashUp);
    String businessDate = StringUtils.EMPTY;
    if (tillStatus.getBusinessdate() != null) {
      businessDate = dateFormat.format(tillStatus.getBusinessdate());
    }
    return businessDate;
  }

  private TerminalSessions getTillStatus(CashUp cashUp) {
    OBCriteria<TerminalSessions> tillStatus = OBDal.getInstance().createCriteria(
        TerminalSessions.class);
    tillStatus.add(Restrictions.eq(TerminalSessions.PROPERTY_CASHUP, cashUp));
    tillStatus.add(Restrictions.eq(TerminalSessions.PROPERTY_POSTERMINAL, cashUp.getPOSTerminal()));
    tillStatus.setMaxResults(1);
    return (TerminalSessions) tillStatus.uniqueResult();
  }

  @Override
  protected boolean itemShouldBeExported(CashUp cashUp) {
    return new AggregatedSalesByPaymentMethod(cashUp).hasSalesToProcess();
  }

  @Override
  public int getPriority() {
    return 90;
  }
}
