/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.sharaf.exporter;

import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.exporter.InvoiceExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = "Invoice")
public class InvoiceCustomExporter extends InvoiceExporter {

  @Override
  public String getSynchronizationQuery() {
    final StringBuilder hql = new StringBuilder();
    hql.append(" SELECT i.id FROM Invoice i ");
    hql.append(" WHERE i.sapobmpIsExported = false ");
    hql.append(" AND i.salesTransaction = true ");
    hql.append(" AND i.$clientCriteria ");
    return hql.toString();
  }

  @Override
  public int getPriority() {
    return 90;
  }
}
