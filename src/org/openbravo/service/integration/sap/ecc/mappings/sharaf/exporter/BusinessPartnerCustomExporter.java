/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.sharaf.exporter;

import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.exporter.BusinessPartnerExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = "BusinessPartner")
/**
 * Custom BusinessPartner exporter for Sharaf. It only exports those that belong to the Customer category
 */
public class BusinessPartnerCustomExporter extends BusinessPartnerExporter {

  private static final String CUSTOMER_CATEGORY = "Customer";

  @Override
  public String getSynchronizationQuery() {
    String standardQuery = super.getSynchronizationQuery();
    return standardQuery + " AND bp.businessPartnerCategory.searchKey = '" + CUSTOMER_CATEGORY
        + "'";
  }

  @Override
  public int getPriority() {
    return 90;
  }
}
