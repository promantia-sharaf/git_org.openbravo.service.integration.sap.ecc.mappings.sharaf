package org.openbravo.service.integration.sap.ecc.mappings.sharaf.component;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

@ApplicationScoped
@IdocType("EDI_DC40_EXTENSION")
public class CasUpWPUTABEDI_DC40ExtensionTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "D337EF269C02433A8029862E4B263A42";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getCashUpId() {
    return StringUtils.defaultIfBlank((String) getProperties().get("RCVLAD"), StringUtils.EMPTY);
  }

}
