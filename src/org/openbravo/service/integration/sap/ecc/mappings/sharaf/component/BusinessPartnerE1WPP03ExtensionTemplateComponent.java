/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.service.integration.sap.ecc.mappings.sharaf.component;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

@ApplicationScoped
@IdocType("E1WPP03_EXTENSION")
public class BusinessPartnerE1WPP03ExtensionTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "CEB2A0D2EAEA4806AD677B0F7EA23BEE";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getEmail() {
    return (String) (getProperties().containsKey("BEMERKUNG") ? getProperties().get("BEMERKUNG")
        : "");
  }

  public String getPhone() {
    return (String) (getProperties().containsKey("FIRMA") ? getProperties().get("FIRMA") : "");
  }

  public String getPostalCode() {
    return (String) (getProperties().containsKey("ABTEILUNG") ? getProperties().get("ABTEILUNG")
        : "");
  }

  public String getAddress() {
    return (String) (getProperties().containsKey("FUNKTION") ? getProperties().get("FUNKTION") : "");
  }

}
