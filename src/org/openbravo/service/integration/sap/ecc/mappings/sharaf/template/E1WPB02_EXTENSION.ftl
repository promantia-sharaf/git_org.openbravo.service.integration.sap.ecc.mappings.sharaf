<#--
/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 ************************************************************************
*/

-->
<VERKAEUFER>${data.salesRepresentative}</VERKAEUFER>
<AKTIONSNR>${data.promotionCode}</AKTIONSNR>
<E1WXX01 SEGMENT="1">
   <FLDGRP>001</FLDGRP>
   <FLDNAME>AUART</FLDNAME>
   <FLDVAL>${data.documentType}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>002</FLDGRP>
   <FLDNAME>VKORG</FLDNAME>
   <FLDVAL>${data.country}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>003</FLDGRP>
   <FLDNAME>VTWEG</FLDNAME>
   <FLDVAL>${data.distributionChannel}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>004</FLDGRP>
   <FLDNAME>TSDT</FLDNAME>
   <FLDVAL>${data.creationDate}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>005</FLDGRP>
   <FLDNAME>TEDT</FLDNAME>
   <FLDVAL>${data.endDate}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>006</FLDGRP>
   <FLDNAME>ZZACT_RCPT</FLDNAME>
   <FLDVAL>${data.originalTicketNumber}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>007</FLDGRP>
   <FLDNAME>ZZACT_RCDT</FLDNAME>
   <FLDVAL>${data.originalTicketDate}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>008</FLDGRP>
   <FLDNAME>ZZAWBNO</FLDNAME>
   <FLDVAL />
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>009</FLDGRP>
   <FLDNAME>ZZSRNO</FLDNAME>
   <FLDVAL />
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>010</FLDGRP>
   <FLDNAME>ECUST</FLDNAME>
   <FLDVAL>${data.endCustomerName}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>011</FLDGRP>
   <FLDNAME>P_DEL_ADD1</FLDNAME>
   <FLDVAL>${data.deliveryAddress}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>012</FLDGRP>
   <FLDNAME>P_DEL_ADD2</FLDNAME>
   <FLDVAL>${data.deliveryAddress2}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>013</FLDGRP>
   <FLDNAME>CONTACT1</FLDNAME>
   <FLDVAL>${data.phoneNumber}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>014</FLDGRP>
   <FLDNAME>CONTACT2</FLDNAME>
   <FLDVAL>${data.phoneNumber2}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>015</FLDGRP>
   <FLDNAME>REMARKS</FLDNAME>
   <FLDVAL>${data.ticketComments}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>016</FLDGRP>
   <FLDNAME>AIRMNO</FLDNAME>
   <FLDVAL>${data.airmilesCardNumber}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>017</FLDGRP>
   <FLDNAME>WEBORDER</FLDNAME>
   <FLDVAL>${data.webOrder}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>101</FLDGRP>
   <FLDNAME>WERKS</FLDNAME>
   <FLDVAL>${data.store}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>102</FLDGRP>
   <FLDNAME>LGORT</FLDNAME>
   <FLDVAL>${data.stockType}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>103</FLDGRP>
   <FLDNAME>RFID</FLDNAME>
   <FLDVAL>${data.rfidCode}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>104</FLDGRP>
   <FLDNAME>AIRMIPT</FLDNAME>
   <FLDVAL>${data.airmilesEarnedPoints}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>105</FLDGRP>
   <FLDNAME>VRKME</FLDNAME>
   <FLDVAL>${data.unitOfMeasure}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>106</FLDGRP>
   <FLDNAME>POSNR</FLDNAME>
   <FLDVAL>${data.lineSequenceNumber}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>107</FLDGRP>
   <FLDNAME>UEPOS</FLDNAME>
   <FLDVAL>${data.parentRecord}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>108</FLDGRP>
   <FLDNAME>RET_RES</FLDNAME>
   <FLDVAL>${data.returnReason}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>109</FLDGRP>
   <FLDNAME>PSTYV</FLDNAME>
   <FLDVAL>${data.deliveryType}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>110</FLDGRP>
   <FLDNAME>P_DEL_DATE</FLDNAME>
   <FLDVAL>${data.ticketDeliveryDate}</FLDVAL>
</E1WXX01>
<E1WXX01 SEGMENT="1">
   <FLDGRP>200</FLDGRP>
   <FLDNAME>VENDOR</FLDNAME>
   <FLDVAL>${data.vendorCode}</FLDVAL>
</E1WXX01>
